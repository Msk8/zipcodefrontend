# Zipcode Frontend

This is a frontend project used in conjunction with ZipcodeAPI, to show and search for zipcodes in Nicaragua

## Languages
This project was made in vue.js 2.5, so it's required to install it in order to use it.


## Installation

Clone the project and in the root directory use the command prompt to install Zipcode Frontend dependencies.

```bash
npm install
```

## Usage

In the command prompt, run to serve at localhost:8080


```
npm run dev
```

## IMPORTANT
Make sure to run the ZipcodeAPI first, otherwise running the project will result in error.